
---------------------------------------------------------------------------
Calisson is a very lightweight PHP service for Calibre
Calisson displays  indexes as a dynamic HTML page
Calisson publishes indexes as ePub, and bookmarks ebooks with Calisson URLs
Calisson serves books bookmarked in the ePub Indexes 
Calisson is published under MIT license
---------------------------------------------------------------------------
Copyright (c) 2018 fabien.battini(AT)gmail.com

+ Targets eReaders, such as Bookeen, Nook etc
  eReaders do not manage efficiently page scrolling
  but are efficient with "page turn"
  So CaliSSon manages both modes
  and also generates index as an ePub

+ the web service:
  + runs as a simple PHP page in an Apache server e.g. from a Synology NAS
  + lets the user download ebooks from the web page
  + lets the user to download a whole serie of books
  + allows the user to customize the Index.epub generated, by either
    allowing selected tags
    rejecting selected tags
    
+ the index.epub generated allows to download ebooks

+ the Calibre directory to be shared between a Windows PC and a Synology NAS 
  administrator can run Calibre on the PC
  administrator can serve epubs and indexes from the NAS, with a lightweight server

+ Manages completely epub (for Android)
+ Manages mobi (for Kindle) only throug web service (no mobi index generated)
  
  
More technical:

+ Configurable through CaliNdex.ini

+ Is pure GET requests,
    so, can be bookmarked
    so, the URL can be stored in an ebook,
    such as the one created by BuildIndex
    the Python-based ePub index generator
    which is included in CaliSSon

+ Allows the administrator to run BuildIndex.py from either Linux or Windows

-------------------------------------------------------------------------
---- Known restrictions 
    
+ allow only 1 TAG per ebook

-------------------------------------------------------------------------


-------------------------------------------------------------------------
---- HOW TO Install for a Synology NAS

Prerequisites:
  Apache HTTP server
  Python 3
  PHP 